var searchData=
[
  ['bomb',['bomb',['../structMoveInfo.html#a2fb56f2b32d4dd99f2712229c4e0045f',1,'MoveInfo']]],
  ['bombid',['bombID',['../structCellInfo.html#a449b3afaaafba495c3e81269367c9133',1,'CellInfo']]],
  ['bombnum',['bombNum',['../structPlayerInfo.html#ae519f6c44682247a0ee2e8ae5e765e70',1,'PlayerInfo::bombNum()'],['../ai__common_8c.html#ac6096e9d1d1eb4c1446bbc6be0200872',1,'bombNum():&#160;ai_common.c']]],
  ['bombownmax',['bombOwnMax',['../structPlayerInfo.html#a9f904837125b526a402849eb26c4c734',1,'PlayerInfo']]],
  ['bombpower',['bombPower',['../structCellInfo.html#af2cdaf7548e57929ba913cdea8b9cfc6',1,'CellInfo']]],
  ['bombs',['bombs',['../ai__common_8c.html#aadde5d256a3d85743cf464491cad1fbf',1,'ai_common.c']]],
  ['burnmap',['BurnMap',['../ai__common_8c.html#a0cfe2689be41518cbbf5f2b3bf180850',1,'ai_common.c']]]
];
