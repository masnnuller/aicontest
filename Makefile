#AIContest C library Makefile
#date 2013/07/27
#author masn19
#mail mas.null19@gmail.com

objs = bin/ai_common.o bin/player.o
bin/aicontest_c_program : $(objs) src/main.c
	clang -Wall -O2 -o bin/aicontest_c_program ${objs} src/main.c
bin/ai_common.o : src/ai_common.c
	clang -Wall -c -o bin/ai_common.o src/ai_common.c
bin/player.o : src/player.c
	clang -Wall -c -o bin/player.o src/player.c

.PHONY: clean
clean:
		rm ${objs} bin/aicontest_c_program
.PHONY: debug
debug: ${objs} src/main.c
	clang -Wall -c -o bin/aicontest_c_program_debug -D DEBUG ${objs} src/main.c


