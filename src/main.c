#include <stdio.h>
#include "ai_common.h"
#include "player.h"

int main(int argc,char **argv)
{
  SetMakeLog(true);
  SetDebug(false);
  printf(CLIENT_NAME);
  printf("\n");
  fflush(stdout);
  OpenLogFile();
  ReadServerInitInfo();
  PrintInitLog();
  AIInit();
  echoPlayerInfo();
  echoMapState();
  int status;
  while(1)
  {
    status = ReadServerInfo();
    PrintFrameLog();
    echoMapState();
    echoPlayerInfo();
    if(status == GAME_END) //Game end
      break;
    MoveInfo aiMove = AIProcess();
    if(status == DISABLE_TO_MOVE)
    {
      NoPutBomb(&aiMove);
      NoMove(&aiMove);
    }
    SendInfo(aiMove);
  }
  ReadGameResult();
  PrintResultLog();
  echoResult();
  CloseLogFile();
  return 0;
}

