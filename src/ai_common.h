/*! @file ai_common.h
 *    @brief メインヘッダ
 * 
 * */

#ifndef AI_COMMON_H_INCLUDED
#define AI_COMMON_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#ifndef _MSC_VER
  #include <stdbool.h>
#endif
//! ステージの縦の長さ
#define STAGE_HEIGHT 11 
//! ステージの横の長さ
#define STAGE_WIDTH 13
//! @defgroup PLAYER_POWER
//! プレイヤーの初期能力値
//! @{

//! プレイヤーの初期火力
#define POWER_INIT 2
//! プレイヤーの最大火力
#define POWER_MAX 8
//! プレイヤーの初期爆弾所持数
#define OWNED_INIT 1
//! プレイヤーの最大爆弾所持数
#define OWNED_MAX 8
//@}

//! 爆弾が設置されてから爆発するまでのフレーム数
#define FRAME_EXPL 10
//! 爆風が消滅するまでのフレーム数
#define FRAME_BURN 3
//! ゲームが終了するフレーム数
#define FRAME_END 600
//! 死亡してから復帰するまでのフレーム数
#define FRAME_REV 20

//!
//! @defgroup OBJECT_ID
//! バトル開始時の情報
//! @{

//! バトル開始時のアイテムの総数
#define ALL_ITEM_NUM 26
//! バトル開始時のソフトブロックの個数
#define NUM_SOFTBLOCK 80
//! バトル開始時のファイアの個数
#define NUM_FIREUP 10
//! バトル開始時のフルファイアの個数
#define NUM_FULLFIRE 2
//! バトル開始時のボムアップの個数
#define NUM_BOMBUP 14
//! @}

//! 各情報のエラー値
#define ERROR_VALUE -100

//! @defgroup OBJECT_TYPE 
//! CellInfoで使われる各種オブジェクトの識別子
//! @{

//! 何もない
#define NONE 0
//! プレイヤー
//! @sa PLAYER_ID
#define PLAYER 1
//! 破壊不能ブロック
#define HARDBLOCK 2
//! 破壊可能ブロック
#define SOFTBLOCK 3
//! 爆弾
#define BOMB 4
//! 爆風
#define FLAME 5
//! アイテム(ファイア)
#define ITEM_FIRE 6
//! アイテム(ボムアップ)
#define ITEM_BOMB 7
//! アイテム(フルファイア)
#define ITEM_FULL 8
//! @}

//! @defgroup SCORE_INFO
//! スコアに関する情報
//! @{

//! 敵プレイヤーを直接キルしたときに入る得点
#define SCORE_KILL 200
//! 敵プレイヤーを誘爆によってキルしたときに入る得点
#define SCORE_SUPPORT 120
//! ソフトブロックを破壊したときに入る得点
#define SCORE_BURN_BLOCK 1
//! @}

//! プレイヤーの最大数
#define PLAYERS_MAX 4

//Move Information
#define MOVE_NONE 0xf0
#define MOVE_LEFT 0x01
#define MOVE_RIGHT 0x02
#define MOVE_UP 0x04
#define MOVE_DOWN 0x08
#define PUT_BOMB 0x10
#define NO_PUT_BOMB 0x0f

#define ENABLE_TO_MOVE 1
#define DISABLE_TO_MOVE 2
#define GAME_END 3

//! @defgroup PLAYER_STATUS
//! プレイヤーの状態を示す識別子
//! @{

//! 生存している
#define PLAYER_ALIVE 0
//! 無敵状態
#define PLAYER_INVINCIBLE 1
//! 爆風で死亡
#define PLAYER_DEAD 2
//! タイムアウト
#define PLAYER_TIMEOUT 3
//! 接続切れで敗北
#define PLAYER_DISCONNECT 4
//! @}
//! 動作を管理する構造体
typedef struct {
  char dir;
  char bomb;
} MoveInfo;

//! 座標を管理する構造体
//! @param x X座標
//! @param y Y座標
typedef struct{
  int x;
  int y;
} Point;

#define POINT_ERROR {ERROR_VALUE,ERROR_VALUE}

//! プレイヤー情報を管理する構造体
//! @param point プレイヤーのいる座標
//! @param status プレイヤーの状態
//! @param score 所持している得点
//! @param power 爆弾の火力
//! @param bombNum 設置している爆弾の個数
//! @param bombOwnMax 所持できる爆弾の最大数
//! @param leastTime 現在の状態の継続時間(無敵状態のときのみ有効)
//! @param lastMove 最後に行った行動
typedef struct{
  Point point;
//! @sa PLAYER_STATUS
  int status;
  int id;
  int score;
  int power;
  int bombNum;
  int bombOwnMax;
  int leastTime;
  MoveInfo lastMove;
} PlayerInfo;

#define PLAYER_ERROR {POINT_ERROR,ERROR_VALUE,ERROR_VALUE,ERROR_VALUE,ERROR_VALUE,ERROR_VALUE,ERROR_VALUE,ERROR_VALUE,{'N','N'}}
//! 爆弾の情報を管理する構造体
//! @param point 爆弾のおいてある座標
//! @param power 爆弾の火力
//! @param leastFrame 爆弾が爆発するまでのフレーム数
//! @param putPlayer 爆弾を設置したプレイヤーのID
typedef struct{
  int leastFrame;
  int power;
  int putPlayer;
  Point point;
} BombInfo;

#define BOMB_ERROR {ERROR_VALUE,ERROR_VALUE,ERROR_VALUE,POINT_ERROR}
//! 爆風の情報を管理する構造体
//! @param leastFrame 爆風が消滅するまでのフレーム数
//! @param located 爆風をそのIDのプレイヤーが発生させたかのフラグ
//! @param locatedPlayer 爆風を発生させたプレイヤーのID
//! @param sourcePos 爆風を発生させた爆弾の座標
typedef struct {
  int leastFrame;
  bool located[PLAYERS_MAX];
  int locatedPlayer[PLAYERS_MAX];
  Point sourcePos[PLAYERS_MAX];
} FlameInfo;

#define FLAME_ERROR {ERROR_VALUE,{false,false,false,false}}
//! マップの1マスについての情報を管理する構造体
//! @param point 座標
//! @param objectType その地点にあるオブジェクト
//! @param bombPower 爆弾がある場合にはその火力、ない場合にはERROR_VALUE
//! @param explosionFrame 爆弾がある場合、爆発するまでのフレーム数、ない場合はERROR_VALUE
//! @param flame 爆風がある場合はその情報、ない場合は各値をERROR_VALUEまたはfalseで埋めたものが入る
//! @param playerFlag マスにそのIDのプレイヤーが存在しているかどうかのフラグ
typedef struct {
  Point point;
//! @sa OBJECT_ID
  int objectType;
  int bombPower;
  int bombID;
  int putPlayerID;
  int explosionFrame;
  FlameInfo flame;
  char playerFlag[PLAYERS_MAX];
} CellInfo;

#define CELL_ERROR {POINT_ERROR,ERROR_VALUE,ERROR_VALUE,ERROR_VALUE,ERROR_VALUE,ERROR_VALUE,FLAME_ERROR,{(char)0,(char)0,(char)0,(char)0}}
//! バトル結果を格納する構造体
//! @param id プレイヤーID
//! @param status バトル終了時の状態
//! @param score 得点
//! @param rank 順位
typedef struct {
  int id;
  int status;
  int score;
  int rank;
} PlayerResult;
//! スコアを格納する構造体
//! @param value 各プレイヤーのスコア
typedef struct {
  int value[PLAYERS_MAX];
} Score;

#define RESULT_ERROR {ERROR_VALUE,ERROR_VALUE,ERROR_VALUE,ERROR_VALUE}
//! 現在のフレーム数を取得する
//! @retval 現在のフレーム数
extern int GetCount();
//! 自分のプレイヤーIDを取得する
//! @return 自分のプレイヤーID
extern int GetPlayerID();
//! 指定したIDのプレイヤー情報を取得する
//! @param id 取得するプレイヤーのID
//! @return 指定したIDのプレイヤー情報
extern PlayerInfo GetPlayerInfo(int id);
//! 指定したIDが有効なプレイヤーIDであるかどうかを取得する
//! @param id プレイヤーID
//! @retval true 指定したIDのプレイヤーが存在する
//! @retval false 指定したIDのプレイヤーは存在しない
extern bool IsValidID(int id);
//! 与えられた座標がマップ内(0<=x<STAGE_WIDTH,0<=y<STAGE_HEIGHT)にあるかを取得する
//! @param p 座標
//! @retval true 座標はマップ内である
//! @retval false 座標はマップ外である
extern bool IsValidPoint(Point p);
//! 与えられた座標にプレイヤーが侵入できるかを取得する
//! @param p 座標
//! @retval true 侵入可能
//! @retval false 侵入不可能
extern bool IsAbleToEnter(Point p);
//! 与えられた座標に爆弾を新しく設置することが可能かを取得する
//! @param p 座標
//! @retval true 設置可能
//! @retval false 設置不可能
extern bool IsAbleToPutBomb(Point p);
//! 与えられた座標に生存した状態で侵入できるかを取得する
//! @param p 座標
//! @retval true 生きたまま侵入可能
//! @retval false 侵入不可能、または侵入すると死亡する
extern bool IsAbleToEnterAlive(Point p);
//! プレイヤーの状態を取得する
//! @param id プレイヤーID
//! @retval 指定したIDのプレイヤーの状態
//! @sa PLAYER_STATUS
extern int GetPlayerState(int id);
//! 与えられたidのプレイヤーが所持している爆弾の個数を取得する
//! @param id プレイヤーID
//! @return 指定したIDのプレイヤーが所持している爆弾の個数
extern int GetPlayerOwnedBomb(int id);
//! 与えられたidのプレイヤーが所持できる爆弾の最大値を取得する
//! @param id プレイヤーID
//! @return 指定したIDのプレイヤーが所持できる爆弾の最大個数
extern int GetPlayerOwnedMaxBomb(int id);
//! 与えられたidのプレイヤーの火力を取得する
//! @param id プレイヤーID
//! @return 指定したIDのプレイヤーが置く爆弾の火力
extern int GetPlayerPower(int id);
//! 与えられたプレイヤーの最後の行動を取得する
//! @param id プレイヤーID
//! @return プレイヤーが最後に行った行動
extern MoveInfo GetPlayerLastMove(int id);
//! 引数として与えたMoveInfoに動かないという情報を与える
//! @param info 上書きされるMoveInfo
//! @return 上書き後のMoveInfo
extern MoveInfo NoMove(MoveInfo *info);
//! 引数として与えたMoveInfoに左に動くという情報を与える
//! @param info 上書きされるMoveInfo
//! @return 上書き後のMoveInfo
extern MoveInfo MoveLeft(MoveInfo *info);
//! 引数として与えたMoveInfoに右に動くという情報を与える
//! @param info 上書きされるMoveInfo
//! @return 上書き後のMoveInfo
extern MoveInfo MoveRight(MoveInfo *info);
//! 引数として与えたMoveInfoに上に動くという情報を与える
//! @param info 上書きされるMoveInfo
//! @return 上書き後のMoveInfo
extern MoveInfo MoveUp(MoveInfo *info);
//! 引数として与えたMoveInfoに下に動くという情報を与える
//! @param info 上書きされるMoveInfo
//! @return 上書き後のMoveInfo
extern MoveInfo MoveDown(MoveInfo *info);
//! 引数として与えたMoveInfoに爆弾を置くという情報を与える
//! @param info 上書きされるMoveInfo
//! @return 上書き後のMoveInfo
extern MoveInfo PutBomb(MoveInfo *info);
//! 引数として与えたMoveInfoに爆弾を置かないという情報を与える
//! @param info 上書きされるMoveInfo
//! @return 上書き後のMoveInfo
extern MoveInfo NoPutBomb(MoveInfo *info);

extern Point UpPoint(Point *p);
extern Point DownPoint(Point *p);
//! 与えられた座標のマスに関する情報を取得する
//! @param p 座標
//! @return その位置にあるマスの情報
extern CellInfo GetCellState(Point p);

bool CheckObj(Point p,int obj);
//! 指定した座標に爆弾があるかを返す
//! @param p 座標
//! @retval true 爆弾が存在する
//! @retval false 爆弾が存在しない
bool CheckBomb(Point p);
//! 指定した座標にアイテムがあるかを返す
//! @param p 座標
//! @retval アイテムがある場合はアイテムのOBJECT_ID
//!   ない場合はERROR_VALUE
//! @sa OBJECT_ID
int CheckItem(Point p);
//! 指定した座標にソフトブロックがあるかを返す
//! @param p 座標
//! @retval true ソフトブロックが存在する
//! @retval false ソフトブロックが存在しない
bool CheckSoftBlock(Point p);
//! 指定した座標にハードブロックがあるかを返す
//! @param p 座標
//! @retval true ハードブロックが存在する
//! @retval false ハードブロックが存在しない
bool CheckHardBlock(Point p);
//! 指定した座標に爆風があるかを返す
//! @param p 座標
//! @retval true 爆風が存在する
//! @retval false 爆風が存在しない
bool CheckFlame(Point p);

//! 現在のマップの配置で何フレーム後にその座標に爆風が到達するかを返す
//! 爆風が到達する場合はその爆風に関するFlameInfoを返す
//! 到達しない場合はERROR_VALUEで埋められたFlameInfoを返す
//! (爆風によるソフトブロックの消滅は未対応です)
//! @param p 座標
//! @return leastFrameに到達までのフレーム数を入れたFlameInfo
extern FlameInfo CheckWillFlame(Point p);
void MakeFlameMap();
int compare_bomb(const void *a,const void *b);
//! 現在出現しているアイテムの総数を返す
//! @return 現在出現しているアイテムの個数
extern int GetAppearItemNum();
//! 現在の各プレイヤーのスコアを返す
//! @return 全プレイヤーのスコアが入ったScore構造体
//! @sa Score
extern Score GetScore();
//! 指定したIDのプレイヤーが設置した爆弾が特定のプレイヤーをキルしたときの得点の推移を計算する
//! @param bombOwner 爆弾の設置者のID
//! @param first ID0のプレイヤーがキルされたことを示すフラグ
//! @param second ID1のプレイヤーがキルされたことを示すフラグ
//! @param third ID2のプレイヤーがキルされたことを示すフラグ
//! @param fourth ID3のプレイヤーがキルされたことを示すフラグ
//! @return 計算された後のScore
extern Score CalcKillScore(int bombOwner,bool first,bool second,bool third,bool fourth);
//! 指定された座標に爆弾がある場合に、それに対応するBombInfoを取得する
//! @param p 座標
//! @return その座標に対応するBombInfo
//!   存在しない場合はERROR_VALUEで各パラメータを埋めたBombInfo
extern BombInfo GetBombInfo(Point p);
//! 指定された座標の爆弾が何フレーム後に爆発するかを取得する
//! 誘爆は考慮しない
//! @param p 座標
//! @return その座標の爆弾が爆発するまでのフレーム数
//!   爆弾が存在しない場合はERROR_VALUE
extern int GetBombExplosionFrame(Point p);
//! 指定された座標の爆風が消滅するまでのフレーム数を取得する
//! @param p 座標
//! @return 座標にある爆風が消滅するまでのフレーム数
//!   爆弾がない場合はERROR_VALUE
extern int GetFlameLeastFrame(Point p);
//! 指定された座標にあるオブジェクトの消滅するまでのフレーム数を返す
//! @param p 座標
//! @return 指定された座標にあるソフトブロック・爆弾・爆風の消滅するまでのフレーム数
extern int GetObjectLife(Point p);
//! 指定した座標にある爆風の情報を取得する
//! @param p 座標
//! @return 指定した座標にある爆風に関するFlameInfo
//!   指定した位置に爆風がない場合は各値をERROR_VALUEまたはfalseで埋めたFlameInfo
extern FlameInfo GetFlameInfo(Point p);
//! 指定されたidのプレイヤーのゲーム結果を取得する
//! @param id プレイヤーID
//! @return 指定したIDのゲーム結果
//! @sa PlayerResult,GetPlayerID()
extern PlayerResult GetResultInfo(int id);

extern void OpenLogFile();
extern void CloseLogFile();
extern void SetMakeLog(int flag);
extern void PrintLog(char *str);
extern void PrintMapLog();
extern void PrintInitLog();
extern void PrintFrameLog();
extern void PrintResultLog();

extern void SetDebug(int flag);
//! 現在マップ上に残っているソフトブロックの総数を取得する
//! @return マップに残っているソフトブロックの数
extern int GetSoftBlockNum();
//! 現在まだ出現していないアイテムの個数を取得する
//! @return 出現していないアイテムの残り数
extern int GetLeastItemNum();

//Read information from server
extern void ReadServerInitInfo();
extern int ReadServerInfo();

//Send data to server
extern void SendInfo(MoveInfo m);
void ReadMapInfo();
void ReadPlayerInfo();
void ReadItemInfo();
void ReadSoftBlockInfo();
void ReadBlockLeastInfo();
void ReadBombInfo();
void ReadFlameInfo();
void ReadGameResult();

extern char GetMoveDir(MoveInfo m);
extern char GetAction(MoveInfo m);

//For Library Debugging
extern void echoMapState();
extern void echoPlayerInfo();
extern void echoResult();

#endif
