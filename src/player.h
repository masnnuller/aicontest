/** @file player.h
 * ここにAIの本体を記述してください。
 * なお、クライアント名を定義することを忘れないようお願いします。
 */
#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED
#include "ai_common.h"
//! クライアント名をここにASKII12文字以下で定義してください
#define CLIENT_NAME "hogehoge"
//! 毎ターンごとに呼び出される関数
//! 次ターンにする動作をMoveInfoとして返してください
MoveInfo AIProcess();
void AIProtocol();
//! バトル開始時に一回のみ呼び出される関数
//! 各種パラメータの初期化等に使用してください
void AIInit();
#endif
