#include "ai_common.h"

PlayerResult results[PLAYERS_MAX];
int myID;
PlayerInfo players[PLAYERS_MAX];
CellInfo map[STAGE_HEIGHT][STAGE_WIDTH];
BombInfo bombs[PLAYERS_MAX * OWNED_MAX];
FILE *logFile;
struct tm *nowTime;
int softBlockNum = 0;
int logFlag = 0;
int turnLogFlag = 1;
int debug;
int MAX_TIME = 600;
int currentTime;


int playerNum;
int appearItemNum;
int leastItemNum;
int bombNum;
FlameInfo BurnMap[STAGE_HEIGHT+2][STAGE_WIDTH+2];

int GetCount()
{
  return currentTime;
}

int GetPlayerID()
{
  return myID;
}

PlayerInfo GetPlayerInfo(int id)
{
  PlayerInfo p = PLAYER_ERROR;
  if(!IsValidID(id))
    return p;
  else{
    if(players[id].id == id)
      return players[id];
  }
  return p;
}

PlayerResult GetResultInfo(int id)
{
  PlayerResult r = RESULT_ERROR;
  if(!IsValidID(id))
    return r;
  return results[id];
}

bool IsValidPoint(Point p)
{
  if(p.x<0 && p.x>=STAGE_WIDTH && p.y<0 && p.y>=STAGE_HEIGHT)
    return false;
  return true;
}

bool IsValidID(int id)
{
  if(id < 0 || id >= PLAYERS_MAX)
    return false;
  return true;
}

bool IsAbleToEnter(Point p)
{
  if(!IsValidPoint(p))
    return false;
  if(map[p.y][p.x].objectType != SOFTBLOCK &&
      map[p.y][p.x].objectType != HARDBLOCK &&
      map[p.y][p.x].objectType != BOMB)
    return true;
  return false;
}

bool IsAbleToPutBomb(Point p)
{
  if(IsAbleToEnter(p) &&
      map[p.y][p.x].objectType != BOMB)
    return true;
  return false;
}

bool IsAbleToEnterAlive(Point p)
{
  if(!IsAbleToEnter(p))
    return false;
  if(map[p.y][p.x].objectType != FLAME)
    return true;
  return false;
}

int GetPlayerState(int id)
{
  if(!IsValidID(id))
    return ERROR_VALUE;
  return players[id].status;
}

int GetPlayerOwnedBomb(int id)
{
  if(!IsValidID(id))
    return ERROR_VALUE;
  return players[id].bombNum;
}

int GetPlayerOwnedMaxBomb(int id)
{
  if(!IsValidID(id))
    return ERROR_VALUE;
  return players[id].bombOwnMax;
}

int GetPlayerPower(int id)
{
  if(!IsValidID(id))
    return ERROR_VALUE;
  return players[id].power;
}

MoveInfo GetPlayerLastMove(int id)
{
  MoveInfo m = {'N','N'};
  if(!IsValidID(id))
    return m;
  return players[id].lastMove;
}

MoveInfo NoMove(MoveInfo *info)
{
  (*info).dir = 'N';
  return *info;
}

MoveInfo MoveLeft(MoveInfo *info)
{
  (*info).dir = 'L';
  return *info;
}

MoveInfo MoveRight(MoveInfo *info)
{
  (*info).dir = 'R';
  return *info;
}

MoveInfo MoveUp(MoveInfo *info)
{
  (*info).dir = 'U';
  return *info;
}

MoveInfo MoveDown(MoveInfo *info)
{
  (*info).dir = 'D';
  return *info;
}


MoveInfo PutBomb(MoveInfo *info)
{
  (*info).bomb = 'B';
  return *info;
}


MoveInfo NoPutBomb(MoveInfo *info)
{
  (*info).bomb = 'N';
  return *info;
}

CellInfo GetCellState(Point p)
{
  CellInfo c = CELL_ERROR;
  if(IsValidPoint(p))
    return c;
  return map[p.y][p.x];
}

int GetObjectLife(Point p)
{
  if(!IsValidPoint(p))
    return ERROR_VALUE;
  return map[p.y][p.x].explosionFrame;
}

bool CheckObj(Point p,int obj)
{
  if(!IsValidPoint(p))
    return ERROR_VALUE;
  if(map[p.y][p.x].objectType == obj)
    return true;
  return false;
}

bool CheckBomb(Point p)
{
  return CheckObj(p,BOMB);
}

bool CheckSoftBlock(Point p)
{
  return CheckObj(p,SOFTBLOCK);
}

bool CheckHardBlock(Point p)
{
  return CheckObj(p,HARDBLOCK);
}

bool CheckFlame(Point p)
{
  return CheckObj(p,FLAME);
}

int CheckItem(Point p)
{
  if(CheckObj(p,ITEM_FULL) || CheckObj(p,ITEM_FIRE) || CheckObj(p,ITEM_BOMB))
    return map[p.y][p.x].objectType;
  return ERROR_VALUE;
}

BombInfo GetBombInfo(Point p)
{
  BombInfo bomb = BOMB_ERROR;
  if(CheckBomb(p))
  {
    bomb.leastFrame = map[p.y][p.x].explosionFrame;
    bomb.power = map[p.y][p.x].bombPower;
    bomb.putPlayer = map[p.y][p.x].putPlayerID;
    bomb.point.x = p.x;
    bomb.point.y = p.y;
  }
  return bomb;
}

int GetBombExplosionFrame(Point p)
{
  if(CheckBomb(p))
    return map[p.y][p.x].explosionFrame;
  return ERROR_VALUE;
}

int GetFlameLeastFrame(Point p)
{
  if(CheckFlame(p))
    return map[p.y][p.x].flame.leastFrame;
  return ERROR_VALUE;
}

int GetSoftBlockNum()
{
  return softBlockNum;
}

int GetLeastItemNum()
{
  return leastItemNum;
}

Score GetScore()
{
  Score s;
  int i;
  for(i = 0;i < PLAYERS_MAX;i++)
  {
    s.value[i] = players[i].score;
  }
  return s;
}

Score CalcKillScore(int bombOwner,bool first,bool second,bool third,bool fourth)
{
  bool flag = {first,second,third,fourth};
  Score s = GetScore();
  if(!IsValidID(bombOwner))
    return s;
  int killcount;
  for(int i = 0;i < PLAYERS_MAX;i++)
    if(flag[i])
      killcount++;
  if(flag[bombOwner])
  {
    killcount--;
    for(int i = 0;i < PLAYERS_MAX;i++)
      if(i != bombOwner)
        s.value[i] += 50 + 250 * (killcount);
  }
  s.value[bombOwner] += 200 * killcount;
  return s;
}

FlameInfo CheckWillFlame(Point p)
{
  return BurnMap[p.y][p.x];
}

void MakeFlameMap()
{
  //マップの初期化
  int i,j,k;
  for(j = 0;j < STAGE_HEIGHT;j++)
  {
    for(i = 0;i < STAGE_WIDTH;i++)
    {
      if(map[j][i].objectType == HARDBLOCK)
      {
        BurnMap[j][i].leastFrame = FRAME_END;
        for(k = 0;k < PLAYERS_MAX;k++)
          BurnMap[j][i].located[k] = 0;
      }else if(map[j][i].objectType == FLAME){
        BurnMap[j][i] = map[j][i].flame;
      }else{
        BurnMap[j][i].leastFrame = FRAME_END;
        for(k = 0;k < PLAYERS_MAX;k++)
          BurnMap[j][i].located[k] = 0;
      }
    }
  }
  int index = 0;
  int dx[4] = {1,0,-1,0},dy[4] = {0,1,0,-1};
  qsort(bombs,bombNum,sizeof(BombInfo),compare_bomb);
  while(index < bombNum)
  {
    for(i = 0;i < 4;i++)
    {
      Point p = bombs[index].point;
      int l = 0;
      while(l < bombs[index].power ||
          IsValidPoint(p) ||
          !CheckHardBlock(p)
          ){
        p.x += dx[i];
        p.y += dy[i];
        if(CheckBomb(p) && map[p.y][p.x].explosionFrame > bombs[index].leastFrame)
        {
          bombs[map[p.y][p.x].bombID].leastFrame = bombs[index].leastFrame;
          break;
        }else if(BurnMap[p.y][p.x].leastFrame > bombs[index].leastFrame){
          BurnMap[p.y][p.x].leastFrame = bombs[index].leastFrame;
          for(k = 0;k < PLAYERS_MAX;k++)
            BurnMap[j][i].located[k] = 0;
          BurnMap[p.y][p.x].located[bombs[index].putPlayer] = 1;
        }
        if(CheckSoftBlock(p)||
            CheckItem(p))
          break;
      }
      l++;
    }
    qsort(bombs+index,bombNum-index,sizeof(BombInfo),compare_bomb);
    index++;
  }
}

int compare_bomb(const void *a,const void *b){
  return ((*(BombInfo*)a).leastFrame - (*(BombInfo*)b).leastFrame);
}

//ログを記録するファイルを開く
void OpenLogFile()
{
  if(!logFlag)
    return;
  time_t timeVal;
  time(&timeVal);
  nowTime = localtime(&timeVal);

  char fileName[512];
  sprintf(fileName,"log_%d%d_%d%d.txt",
      nowTime->tm_year - 100,nowTime->tm_mon + 1,
      nowTime->tm_hour,nowTime->tm_min);
  logFile = fopen(fileName,"w");
  if(logFile == NULL)
    logFile = stderr;
}

//ログを記録するファイルを閉じる。
void CloseLogFile()
{
 if(logFile != stderr && logFile != NULL)
   fclose(logFile);
}

//ログ出力の設定を行う
//1 = ログを出力 0 = ログを出力しない
void SetMakeLog(int flag)
{
  logFlag = flag;
}

//任意の文字列をログファイルに出力する
//ログ出力の設定をしていない場合は出力しない
void PrintLog(char *str)
{
  if(!logFlag)
    return;
  fprintf(logFile,"\t[USER] \"%s\" \n",str);
}

//毎フレームのログを出力する
void PrintFrameLog()
{
  if(!logFlag && !turnLogFlag)
    return;
  if(GetPlayerInfo(GetPlayerID()).status == PLAYER_ALIVE)
  {
    char c1,c2;
    c1 = players[GetPlayerID()].lastMove.dir;
    c2 = players[GetPlayerID()].lastMove.bomb;
    fprintf(logFile,"\tFrame %d : ",GetCount());
    if(c2 == 'N')
      fprintf(logFile, "%c\n",c1);
    else
      fprintf(logFile, "%c%c\n",c2,c1);
  }
  if(GetPlayerInfo(GetPlayerID()).status == PLAYER_INVINCIBLE)
  {
    turnLogFlag = 0;
    fprintf(logFile,"\tFrame %d : Invincible\n",GetCount());
  }
  if(GetPlayerInfo(GetPlayerID()).status == PLAYER_DEAD)
  {
    turnLogFlag = 0;
    fprintf(logFile,"\tFrame %d : !!Player Dead!!\n",GetCount());
  }
  if(GetPlayerInfo(GetPlayerID()).status == PLAYER_DISCONNECT)
  {
    turnLogFlag = 0;
    fprintf(logFile,"\tFrame %d : PLAYER_DISCONNECT\n",GetCount());
  }
}

//ゲーム開始時のログを出力する
void PrintInitLog()
{
  if(!logFlag)
    return;
  fprintf(logFile,"[INFO]\n");
  fprintf(logFile,"START_TIME : %d/%d/%d/%d:%d\n",
      nowTime->tm_year - 100,nowTime->tm_mon + 1,
      nowTime->tm_mday,nowTime->tm_hour,
      nowTime->tm_min);
  fprintf(logFile,"[Log]\n");
}

//ゲーム終了後にリザルトをログファイルに書き込む
void PrintResultLog()
{
  char pst[4][20] = {"PLAYER_FIRST ","PLAYER_SECOND","PLAYER_THIRD ","PLAYER_FOURTH"};
  PlayerResult r;
  if(!logFlag)
    return;
  fprintf(logFile,"[Result]\n");
  int i;
  for(i = 0;i < PLAYERS_MAX;i++)
  {
    switch((r = GetResultInfo(i)).status)
    {
      case PLAYER_TIMEOUT:
        fprintf(logFile,"\t%s %d : TIME_OUT %d\n",pst[i],r.rank,r.score);
        break;
      case PLAYER_DISCONNECT:
        fprintf(logFile,"\t%s %d : DISCONNECT %d\n",pst[i],r.rank,r.score);
        break;
      default:
        fprintf(logFile,"\t%s %d : ALIVE %d\n",pst[i],r.rank,r.score);
    }
  }
}

//Debug設定(デフォルトで無効)
void SetDebug(int flag)
{
  debug = flag;
}


//サーバーからゲーム開始時の情報を取得する
void ReadServerInitInfo()
{
  char s[10];
  int W,H;
  scanf("%d",&myID);
  scanf("%s",s);
  scanf("%d%d",&W,&H);
  scanf("%d",&MAX_TIME);
  ReadMapInfo();
  ReadPlayerInfo();
  currentTime = 0;
  if(debug)
    fprintf(stderr,"InitInfo Done.\n");
}

//フレームごとにサーバーから情報を取得する
int ReadServerInfo()
{
  int _status = 0;
  scanf("%d",&_status);
  if(_status == GAME_END)
    return GAME_END;
  else if(_status < 1 || _status > GAME_END)
    exit(1);
  ReadMapInfo();
  ReadSoftBlockInfo();
  ReadBombInfo();
  ReadFlameInfo();
  ReadItemInfo();
  ReadPlayerInfo();
  MakeFlameMap();
  scanf("%d",&currentTime);
  return _status;
}

void ReadMapInfo()
{
  int i,j;
  char buf[256];
  softBlockNum = 0;
  appearItemNum = 0;
  for(i = 0;i < STAGE_HEIGHT;i++)
  { 
    scanf("%s",buf);
    for(j = 0;j< STAGE_WIDTH;j++)
    {
      int k;
      map[i][j].objectType = NONE;
      map[i][j].bombPower = ERROR_VALUE;
      map[i][j].putPlayerID = ERROR_VALUE;
      map[i][j].explosionFrame = ERROR_VALUE;
      map[i][j].flame.leastFrame = ERROR_VALUE;
      for(k = 0;k < PLAYERS_MAX;k++)
      {  
        map[i][j].flame.located[k] = false;
        map[i][j].flame.locatedPlayer[k] = ERROR_VALUE;
        map[i][j].flame.sourcePos[k].x = ERROR_VALUE;
        map[i][j].flame.sourcePos[k].y = ERROR_VALUE;
      }
      map[i][j].point.x = j;
      map[i][j].point.y = i;
      for(k = 0;k < 4;k++)
        map[i][j].playerFlag[k] = 0;
      if(buf[j] == '.')
      {
        //None to do
      } else if(buf[j] == '@')
      {
        map[i][j].objectType = SOFTBLOCK;
        softBlockNum++;
      } else if(buf[j] == '#')
      {
        map[i][j].objectType = HARDBLOCK;
      }else if(buf[j] == '+'){
        appearItemNum = appearItemNum + 1;
      }else if(buf[j] == '*'){
        map[i][j].objectType = BOMB;
      }else{
        map[i][j].objectType = FLAME;
        map[i][j].flame.leastFrame = (int)(buf[j] - '0');
        map[i][j].explosionFrame = map[i][j].flame.leastFrame;
      }
    }
  }
  if(debug)
    fprintf(stderr,"MapInfo Done.\n");
} 

void ReadFlameInfo()
{
  int fnum,i,j,x,y,ltime,inum;
  scanf("%d",&fnum);
  for(i = 0;i < fnum;i++)
  {
    scanf("%d %d %d %d",&y,&x,&ltime,&inum);
    map[y][x].flame.leastFrame = ltime;
    for(j = 0;j < inum;j++)
    {
      int b,lx,ly;
      scanf("%d%d%d",&b,&lx,&ly);
      map[y][x].flame.located[b] = true;
      map[y][x].flame.locatedPlayer[j] = b;
      map[y][x].flame.sourcePos[j].x = lx;
      map[y][x].flame.sourcePos[j].y = ly;
    }
  }
  if(debug)
    fprintf(stderr,"FlameInfo Done\n");
}

//Get Player Information From Server
void ReadPlayerInfo()
{
  //maybe playernum
  int i;
  int x,y;
  char buf[5];
  MoveInfo m;
  NoPutBomb(&m);
  NoMove(&m);
  scanf("%d",&playerNum);
  for(i = 0;i < playerNum;i++)
  {
    scanf("%d %d %d %d %d %d %d %d %s",&y,&x,&(players[i].status),&(players[i].id),&(players[i].bombNum),&(players[i].bombOwnMax),&(players[i].power),&(players[i].score),buf);
    map[y][x].objectType = PLAYER;
    players[i].point.x = x;
    players[i].point.y = y;
    players[i].lastMove.dir = 'N';
    players[i].lastMove.bomb = 'N';
    map[y][x].playerFlag[players[i].id] = 1;
    players[i].leastTime = ERROR_VALUE;
    if(buf[0] == 'B')
    {
      PutBomb(&(players[i].lastMove));
      buf[0] = buf[1];
    }
    players[i].lastMove.dir = buf[0];
  }
  int axn,id,t,j;
  for(j = 0;j < 2;j++)
  {
    scanf("%d",&axn);
    for(i = 0;i < axn;i++)
    {
      scanf("%d%d%d%d",&y,&x,&id,&t);
      players[id].leastTime = t;
    }
  }
  if(debug)
    fprintf(stderr,"PlayerInfo Done.\n");
}

void ReadItemInfo()
{
  scanf("%d%d",&leastItemNum,&appearItemNum);
  int i,x,y,type;
  for(i = 0;i < appearItemNum;i++)
  {
    scanf("%d%d%d",&y,&x,&type);
    if(type == 0)
      map[y][x].objectType = ITEM_FIRE;
    if(type == 1)
      map[y][x].objectType = ITEM_BOMB;
    if(type == 2)
      map[y][x].objectType = ITEM_FULL;
  }
  if(debug)
  fprintf(stderr,"ItemInfo Done.\n");
}

void ReadBombInfo()
{
  int i,x,y,time,power,id;
  scanf("%d",&bombNum);
  for(i = 0;i < bombNum; i++)
  {
    scanf("%d%d%d%d%d",&y,&x,&time,&power,&id);
    map[y][x].objectType = BOMB;
    map[y][x].bombPower = power;
    map[y][x].putPlayerID = id;
    map[y][x].explosionFrame = time;
    map[y][x].bombID = i;
    bombs[i].leastFrame = time;
    bombs[i].power = power;
    bombs[i].putPlayer = id;
    bombs[i].point.x = x;
    bombs[i].point.y = y;
  }
  if(debug)
    fprintf(stderr,"BombInfo Done.\n");
}

void ReadSoftBlockInfo()
{
  int k,i,x,y,t;
  scanf("%d",&k);
  for(i = 0;i < k;i++)
  {
    scanf("%d%d%d",&y,&x,&t);
    map[y][x].explosionFrame = t;
  }
  if(debug)
    fprintf(stderr,"SoftBlockInfo Done.\n");
}

void ReadGameResult()
{
  int pCount,i;
  scanf("%d",&pCount);
  for(i = 0;i < pCount;i++)
  {
    scanf("%d%d%d%d",&(results[i].id),&(results[i].status),&(results[i].rank),&(results[i].score));
  }
  if(debug)
    fprintf(stderr,"Result Done.\n");
}

char GetMoveDir(MoveInfo m)
{
  return m.dir;
}

char GetAction(MoveInfo m)
{
  return m.bomb;
}

void SendInfo(MoveInfo m)
{
  char a,b;
  a = GetMoveDir(m);
  b = GetAction(m);
  if(b == 'N')
    printf("%c\n",a);
  else
    printf("%c%c\n",b,a);
  fflush(stdout);
}



void echoMapState()
{
  if(!debug)
    return;
  int i,j;
  char c;
  fprintf(stderr,"---MapInfo for debug---\n");
  for(i = 0;i < STAGE_HEIGHT;i++)
  {  
    for(j = 0;j < STAGE_WIDTH;j++)
    {
      switch(map[i][j].objectType)
      {
        case PLAYER:
        case NONE:
          c = '.';
          break;
        case BOMB:
          c = '*';
          break;
        case ITEM_BOMB:
        case ITEM_FIRE:
        case ITEM_FULL:
          c = '+';
          break;
        case FLAME:
          c = (char)('0' + map[i][j].flame.leastFrame);
          break;
        case HARDBLOCK:
          c = '#';
            break;
        case SOFTBLOCK:
          c = '@';
          break;
      }
      fprintf(stderr,"%c ",c);
    }
    fprintf(stderr,"\n");
  }
  fprintf(stderr,"---MapInfo for debug end---\n");
}

void echoPlayerInfo()
{
  if(!debug)
    return;
  fprintf(stderr,"---PlayerInfo for debug---\n");
  int i;
  for(i = 0;i<playerNum;i++)
  {
    fprintf(stderr,"Player ID: %d\n",players[i].id);
    fprintf(stderr,"Player Status: %d\n",players[i].status);
    fprintf(stderr,"Player Score: %d\n",players[i].score);
    fprintf(stderr,"Player Power: %d\n",players[i].power);
    fprintf(stderr,"Player bombNum: %d\n",players[i].bombNum);
    fprintf(stderr,"Player bombOwnMax: %d\n",players[i].bombOwnMax);
    fprintf(stderr,"Player lastMove: %c %c\n",players[i].lastMove.bomb,players[i].lastMove.dir);
  }
  fprintf(stderr,"---PlayerInfo for debug end---\n");
}

void echoResult()
{
  if(!debug)
    return;
  fprintf(stderr,"---ResultInfo for debug---\n");
  int i;
  for(i = 0;i < playerNum;i++){
    fprintf(stderr,"Player %d: ",results[i].id);
    if(results[i].status == PLAYER_INVINCIBLE)
      fprintf(stderr,"PLAYER_INVINCIBLE %d\n",results[i].score);
    else if(results[i].status == PLAYER_TIMEOUT)
      fprintf(stderr,"PLAYER_TIMEOUT : %d\n",results[i].score);
    else if(results[i].status == PLAYER_DISCONNECT)
      fprintf(stderr,"PLAYER_DISCONNECT : %d\n",results[i].score);
    else
      fprintf(stderr,"PLAYER_ALIVE : %d\n",results[i].score);
  }
  fprintf(stderr,"---ResultInfo for debug end---\n");
}
